import os
import numpy as np


def save_data(dir_path, inputs_data, start_name="input"):
    res = []
    for i, data in enumerate(inputs_data):
        data_path = os.path.join(dir_path, "%s_%s.npy" % (start_name, i))
        np.save(data_path, data)
        res.append(data_path)
    return res


def load_data(inputs_path):
    if isinstance(inputs_path[0], np.ndarray):
        return inputs_path
    else:
        inputs_data = []
        for data_path in inputs_path:
            data = np.load(data_path)
            inputs_data.append(data)
        return inputs_data
