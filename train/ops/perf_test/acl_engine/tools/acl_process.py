import subprocess


def acl_run(op_type, inputs_path, inputs_info, outputs_info, attr, device_id, op_dir, is_dynamic=0):
    cmd = ["python3", "tools/acl_execute.py"]
    params = ["--op_type=%s" % op_type, "--inputs_path=%s" % str(inputs_path), "--inputs_info=%s" % str(inputs_info),
              "--outputs_info=%s" % str(outputs_info), "--attrs=%s" % str(attr), "--device_id=%s" % device_id,
              "--op_model_path=%s" % op_dir, "--is_dynamic=%s" % is_dynamic]
    cmd.extend(params)
    try:
        subprocess.run(cmd, timeout=600)
    except Exception as e:
        print(e)
