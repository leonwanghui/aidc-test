# 简介
acl_engine工具用来测试cann单算子性能，工具会将单算子转换为om离线模型，然后调用acl接口执行单算子推理，工具会统计算子下发到推理完成的耗时。

# 环境搭建
  1. 参考昇腾社区的开发环境搭建文档，完成开发环境搭建，文档链接：https://www.hiascend.com/document/detail/zh/canncommercial/60RC1/envdeployment/instg/instg_000018.html
  2. 本次测试使用社区版本6.0.0.alpha002，版本链接：https://www.hiascend.com/software/cann/community，cann包Ascend-cann-toolkit_6.0.0.alpha002_linux-aarch64.run
  3. 为方便测试，测试环境已制作docker镜像，镜像名称为operator:aidc-test

# 使用指导
  ### 下载测试脚本
  ```shell
  git clone https://gitee.com/leonwanghui/aidc-test.git
  ```
  ### 进入测试目录
  ```shell
  cd train/ops/perf_test/acl_engine
  ```
  测试依赖算子输入文件conv_f16.csv和gemm_f16.csv文件，请提前准备并放在acl_engine目录下。
  ### 启动容器测试：
  ```shell
  bash run_optest.sh
  ```
  ### 进入测试脚本目录
  ```shell
  cd ***/aidc-test/train/ops/perf_test/acl_engine  #***是自己下载代码所在目录
  ```

  ### 设置环境变量
  ```shell
  source env.sh
  ```

  ### 执行算子性能测试：
  soc_version根据产品形态变动，当前使用Ascend910B
  ```shell
  Conv2D性能测试
   python3 acl_conv2d.py --file=conv_f16.csv --soc_version=Ascend910B
   测试结果写入result_conv_f16.csv 文件
  GEMM性能测试
   python3 acl_gemm.py --file=gemm_f16.csv --soc_version=Ascend910B
   测试结果写入result_gemm_f16.csv 文件
   ```