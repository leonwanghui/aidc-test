from long_tail_bench.api import run, set_running_config
import torch
import torch_npu
__all__ = ["run", "set_running_config"]
