import os
from long_tail_bench.common import auto_register
import torch
import torch_npu
auto_register(os.path.dirname(os.path.realpath(__file__)), __name__)
