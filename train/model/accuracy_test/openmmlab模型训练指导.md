## openmmlab模型训练指导



#### 容器化部署

##### 启动容器

```shell
bash /home/chenzhgming.vendor/scripts/run_openmmlab.sh
```

##### 创建软链接

```shell
ln -s /usr/bin/python3 /usr/bin/python
```

##### 关闭集群通信白名单

```shell
export HCCL_WHITELIST_DISABLE=1
```

##### 进入模型目录

```shell
cd /home/chenzhgming.vendor/mmclassification-0.24.1
```

##### 将数据集软链接到data目录下

```
ln -s /mnt/datasets/Imagenet ./data/imagenet
```

##### 启动训练脚本

```shell
bash ./tools/dist_train.sh configs/resnet/resnet50_8xb32_in1k.py 8 --device npu
```



#### 镜像打包流程

##### 一、启动基础镜像

```shell
bash /home/chenzhgming.vendor/scripts/run.sh
```

##### 二、安装mmclassification

```shell
git clone -b v0.24.1  https://github.com/open-mmlab/mmclassification.git
cd mmclassification
pip3 install -e .
```

##### 三、安装npu版本的mmcv

参考链接：https://github.com/open-mmlab/mmcv/blob/master/docs/zh_cn/get_started/build.md

1.Ascend 编译版本的 mmcv-full 在 mmcv >= 1.7.0 时已经支持直接 pip 安装

```shell
pip install mmcv-full -f https://download.openmmlab.com/mmcv/dist/ascend/torch1.8.0/index.html
```

2.源码编译

```
# 获取源码
git clone -b v1.7.0 --depth=1 https://github.com/open-mmlab/mmcv.git
# 编译
MMCV_WITH_OPS=1 MAX_JOBS=8 FORCE_NPU=1 python setup.py build_ext
# 安装
MMCV_WITH_OPS=1 FORCE_NPU=1 python setup.py develop
```

##### 四、打包镜像

```
docker commit 容器id 镜像名称:tag
```



