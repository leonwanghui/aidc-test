# 带宽测试
带宽测试包括单NPU、双NPU间的带宽测试以及NPU的访存带宽测试

## 目录结构

```
bandwidth
|----bandwidthtest.sh
|----env_set.sh
|----result_npu.txt
|____readme.md
```

`bandwidthtest.sh`是进行测试的脚本

`env_set.sh`是设置环境变量的脚本

`result_npu.txt`是在NPU测试的结果

## 运行测试
设置环境变量：source env_set.sh

```
source env_set.sh
bash bandwidthtest.sh
```

