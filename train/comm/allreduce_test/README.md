# all_reduce测试
本测试采用昇腾hccl_test测试工具，在不同通信负载和通信节点条件下，测试All-Reduce 算子（使用sum op）的带宽和时延。
需要安装mpich-3.2.1，以及CANN 6.0.0.alpha002，下载地址https://www.hiascend.com/software/cann/community

### 编译
1.hccl_test依赖MPI拉起多个进程，需配置MPI_HOME为MPI安装路径，并导入MPI库路径
```
export MPI_home=/path/to/mpi
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/path/to/mpi/lib
```
2.导入集合通信库路径
```
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/Asecnd/latest/lib64
```
3.编译六种算子的可执行文件
在CANN安装路径${install_path}/tools/hccl_test下创建build
```
cd  ${install_path}/tools/hccl_test
mkdir build && cd build
cmake ..
make
```

### 拷贝所有脚本至指定文件夹
```
cp * /usr/local/Ascend/ascend-toolkit/latest/tools/hccl_test/bin
cd /usr/local/Ascend/ascend-toolkit/latest/tools/hccl_test/bin
```
### 文件目录
```
|--bin
|----test.sh
|----set_env.sh
|----run.sh
|----hostfile_16
|----hostfile_32
|----all_reduce_test
|____allreduce_result.txt
```

set_env.sh配置相关的环境变量
test.sh为测试执行脚本
hostfile_16，hostfile_32为参与测试的机器，配置ip和npu卡数
run.sh配置执行脚本的机器ip

### 设置环境变量
```
source set_env.sh
```

### 启动测试脚本
```
bash test.sh > allreduce_result.txt
```

### 参数说明
1.数据量范围设置
```
 -b, --minbytes<min counts> 起始数据量，默认值：20M
 -e, --maxbytes<min counts> 结束数据量，默认值：30M
```

2.规约类型配置（sum, max, min, prod）
```
 -o,--op <sum/max/min/prod>默认值:sum
```

3.测试轮次设置
```
-n,--iters<iteration count> 执行轮次，默认值：20
```
4.npu个数
```
 -p,--npus<npus used for one node>每个计算节点上，参与训练的npu个数，默认值：8
```
