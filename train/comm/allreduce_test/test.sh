function test_iter()
{
    for npu in 2 4 8 16 32
    do
        echo "######## npu = ${npu} ########"
        for num in 300000 3097600 6553600 16777216 134217700
        do
            if [ $npu -gt 8 ];
            then
                mpirun -n ${npu} -f hostfile_${npu} ./run.sh "./all_reduce_test -b ${num} -e ${num} -p 8"
            else
                mpirun -n ${npu} ./all_reduce_test -b ${num} -e ${num} -p ${npu}
            fi
        done
    done
}

test_iter
