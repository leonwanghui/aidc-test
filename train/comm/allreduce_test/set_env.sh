export MPI_HOME=/usr/local/mpich-3.2.1/
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/mpich-3.2.1/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/Ascend/ascend-toolkit/latest/lib64

export LD_LIBRARY_PATH=/usr/local/gcc7.3.0/lib64:$LD_LIBRARY_PATH
export PATH=/usr/local/mpich-3.2.1/bin:$PATH
export PATH=/usr/local/gcc7.3.0/bin:$PATH
