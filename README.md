# aidc-test

**注意**：该代码仓仅用于POC测试，未经授权，禁止用于商业用途。

本次测试覆盖昇腾训练和推理端到端场景，测试项包括带宽测试、集群通信、算子和模型等方面。当前仓库目录组织如下：
```
aidc-test/
├── infer
│   ├── dvpp              # 图像编解码
│   ├── model             # 模型推理
│   ├── ops               # 算子测试
│   └── others
└── train
    ├── comm
    │   ├── bandwidth_test # 带宽测试
    │   ├── allreduce_test # 集群通信测试    
    ├── model
    │   ├── accuracy_test # 模型功能测试
    │   └── perf_test     # 模型性能测试
    ├── ops
    │   ├── accuracy_test # 算子功能测试
    │   └── perf_test     # 算子性能测试
    └── others
```
